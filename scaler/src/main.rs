extern crate scaler;
use scaler::image::*;
#[macro_use]
use scaler::*;
use std::io::Write;

fn main() {
    let mut args = std::env::args();
    // Skip program name
    if let Some(prog_name) = args.next() {
        if let Some(mut path) = args.next() {
            match Image::load(&path) {
                Ok(img) => {
                    let filename = if let Some(lastperiod) = path.rfind('.') {
                        path.split_off(lastperiod);
                        path
                    } else {
                        path
                    };

                    let grey_path = format!("{}_greyscale.png", filename);
                    let greyscale = img.to_greyscale();
                    err!(greyscale.save(&grey_path));
                    let differential_kernel_x = vec![-1.0, 0.0, 1.0,
                                                   -2.0, 0.0, 2.0,
                                                   -1.0, 0.0, 1.0];

                    let differential_kernel_y = vec![-1., -2., -1.,
                                                      0.,  0.,  0.,
                                                      1.,  2.,  1.];

                    let diff_y = greyscale.apply_kernel(&differential_kernel_y, 3, 3);
                    let diff_x = greyscale.apply_kernel(&differential_kernel_x, 3, 3);  
                    let two_norm = |a: f32, b: f32| a*a+b*b;
                    let diff_two = diff_x.combine(&diff_y, two_norm);
                    let diff_path = format!("{}_differential.png", filename);
                    err!(diff_two.save(&diff_path));

                    let seam = diff_two.find_shortest_row(&None);
                    let seam_path = format!("{}_seam.png", filename);
                    err!(err!(img.visualise_seam(&seam)).save(&seam_path));

                    let removed_seam_path = format!("{}_removed_seam.png", filename);
                    let mut last_hint: Option<usize> = seam.get(0).map(|x| *x);

                    let mut new_img = img.remove_seam(&seam);
                    err!(new_img.save(&removed_seam_path));

                    let begin = std::time::Instant::now();
                    for i in 1..1500 {
                        let new_greyscale = img.to_greyscale();
                        let new_differential_x = new_greyscale.apply_kernel(&differential_kernel_x, 3, 3);
                        let new_differential_y = new_greyscale.apply_kernel(&differential_kernel_y, 3, 3);
                        let new_differential = new_differential_x.combine(&new_differential_y, two_norm);
                        let new_seam = new_differential.find_shortest_row(&last_hint);
                        new_img = new_img.remove_seam(&new_seam);
                        print!("\r{}", i);
                        std::io::stdout().flush();
                        last_hint = new_seam.get(0).map(|x| *x);
                    }
                    println!();
                    let duration = std::time::Instant::now()-begin;
                    let final_path = format!("{}_final.png", filename);
                    err!(new_img.save(&final_path));
                    println!("{:?}", duration);
                }
                Err(e) => println!("{}", e),
            }
        } else {
            // TODO: clean up
            println!("Usage {} <path to file>", prog_name)
        }
    }
}
