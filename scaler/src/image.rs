use std::error::Error;
use std::fmt::Display;
use std::fs::File;
use std::sync::Arc;

extern crate png;
use png::Decoder;
use std::io::BufWriter;

use std::collections::{BinaryHeap, HashMap};

extern crate crossbeam;

#[derive(Debug)]
enum ImageError {
    ColourNotSupported,
}

impl Error for ImageError {}

impl Display for ImageError {
    fn fmt(&self, formatter: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(formatter, "{:?}", self)
    }
}

const THRESHOLD: u16 = 40000;
const NUM_THREADS: usize = 16;

type Colour = f32;
type Seam = Vec<usize>;

pub struct Image {
    underlying: Vec<Colour>,
    width: usize,
    height: usize,
    rgb: bool,
}

#[derive(PartialEq, Clone, Debug)]
struct Node {
    previous: Option<usize>,
    distance: f64,
    id: usize,
}

impl Node {
    fn successors(&self, width: usize, height: usize) -> Vec<usize> {
        if self.id == 0 {
            (1..width + 1).collect()
        } else if self.id <= width * (height - 1) {
            let mut result = Vec::new();
            let center = self.id + width;

            let y = (center - 1) / width;
            let x = (center - 1) % width;
            let is = vec![1, 0, 2];
            for i in is {
                let potential_neighbour = center + i - 1;

                // check if the potential neighbour is on the same y as the center
                if (potential_neighbour - 1) / width == y {
                    let pot_neighbour_x = (potential_neighbour - 1) % width;
                    assert!(
                        pot_neighbour_x as i64 - x as i64 <= 1 || x - pot_neighbour_x <= 1,
                        format!("{} vs {}", potential_neighbour, self.id)
                    );
                    result.push(potential_neighbour);
                }
            }
            result
        } else if self.id > width * (height - 1) && self.id <= width * height {
            vec![width * height + 1]
        } else {
            vec![]
        }
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Node) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Node {
    fn cmp(&self, other: &Node) -> std::cmp::Ordering {
        if self.distance < other.distance {
            std::cmp::Ordering::Greater
        } else if other.distance < self.distance {
            std::cmp::Ordering::Less
        } else {
            std::cmp::Ordering::Equal
        }
    }
}

impl Eq for Node {}

// This is dumb
const NUM_COLOURS_RGB: usize = 3;
const NUM_COLOURS_GREY: usize = 1;

impl Image {
    pub fn print(&self) {
        let num_colours = if self.rgb {
            NUM_COLOURS_RGB
        } else {
            NUM_COLOURS_GREY
        };

        let mut counter = 0;
        println!("Number of channels: {}", num_colours);
        for c in &self.underlying {
            print!("{:.03} ", c);

            counter += 1;
            if counter % (self.width * num_colours) == 0 {
                counter = 0;
                println!();
            }
        }
    }

    pub fn with_data(&self, new_data: Vec<Colour>) -> Image {
        Image {
            width: self.width,
            height: self.height,
            rgb: self.rgb,
            underlying: new_data,
        }
    }

    pub fn visualise_seam(&self, seam: &Seam) -> Result<Image, String> {
        if seam.len() != self.height {
            Err("Seam length did not match image height".to_string())
        } else {
            let num_colours = self.num_colours();
            let mut underlying = self.underlying.clone();
            for (i, idx) in seam.iter().enumerate() {
                for c in 0..num_colours {
                    *underlying
                        .get_mut((i * self.width + idx) * num_colours + c)
                        .expect("invalid index?") = 1.0
                }
            }
            Ok(self.with_data(underlying))
        }
    }

    fn clamp<T: std::cmp::PartialOrd>(val: T, min: T, max: T) -> T {
        if val < min {
            min
        } else if val > max {
            max
        } else {
            val
        }
    }

    fn vec_u8_to_f32(arg: &Vec<u8>) -> Vec<Colour> {
        let mut result: Vec<Colour> = Vec::with_capacity(arg.len());
        for c in arg {
            let float_c: Colour = (*c).into();
            let float_max_val: Colour = u8::max_value().into();
            result.push(float_c / float_max_val);
        }
        result
    }

    fn vec_f32_to_u8(arg: &Vec<Colour>) -> Vec<u8> {
        let mut result: Vec<u8> = Vec::with_capacity(arg.len());
        for c in arg {
            let clamped = Image::clamp(*c, 0., 1.);
            let float_max_val: Colour = u8::max_value().into();
            result.push((clamped * float_max_val) as u8);
        }
        result
    }

    pub fn load(path: &str) -> Result<Image, Box<dyn Error>> {
        let decoder = Decoder::new(File::open(path)?);
        let (info, mut reader) = decoder.read_info()?;
        let mut buffer = vec![0; info.buffer_size()];
        reader.next_frame(&mut buffer)?;
        let width = info.width as usize;
        let height = info.height as usize;
        if let png::ColorType::RGB = info.color_type {
            if let png::BitDepth::Eight = info.bit_depth {
                // TODO: add greyscale support
                let underlying = Image::vec_u8_to_f32(&buffer);
                Ok(Image {
                    underlying,
                    width,
                    height,
                    rgb: true,
                })
            } else {
                println!("{:?}", info.bit_depth);
                Err(Box::new(ImageError::ColourNotSupported))
            }
        } else {
            println!("{:?}", info.color_type);
            Err(Box::new(ImageError::ColourNotSupported))
        }
    }

    pub fn apply_kernel(
        &self,
        kernel: &Vec<f32>,
        k_width: usize,
        k_height: usize,
    ) -> Image {
        if k_height * k_width == 0 {
            panic!("Kernel height or width zero");
        }

        if k_height * k_width != kernel.len() {
            panic!("Kernel's size doesn't match height and width");
        }

        let k_width_i = k_width as i64;
        let k_height_i = k_height as i64;

        if self.rgb {
            panic!("Cannot invoke kernel on rbg image");
        }

        let num_elems = self.height * self.width;
        let mut underlying: Vec<f32> = vec![0.0; num_elems];
        let chunk_size = (num_elems - 1 + NUM_THREADS) / NUM_THREADS;

        let chunks = underlying.chunks_mut(chunk_size);
        crossbeam::thread::scope(|scope| {
            for (chunk_idx, chunk) in chunks.enumerate() {
                scope.spawn(move |_| {
                    for (elem_idx, elem) in chunk.iter_mut().enumerate() {
                        let global_idx = chunk_idx * chunk_size + elem_idx;
                        let global_x = (global_idx % self.width) as i64;
                        let global_y = (global_idx / self.width) as i64;
                        for k_y in 0..k_height {
                            for k_x in 0..k_width {
                                let actual_x =
                                    Image::clamp(global_x - k_width_i / 2 + k_x as i64, 0, self.width as i64 - 1) as usize;
                                let actual_y =
                                    Image::clamp(global_y - k_height_i / 2 + k_y as i64, 0, self.height as i64 - 1) as usize;
                                let actual_idx = actual_x + actual_y * self.width;

                                let additional = unsafe {
                                    kernel.get_unchecked(k_x + k_y * k_width)
                                        * self.underlying.get_unchecked(actual_idx)
                                };
                                *elem += additional;
                            }
                        }
                    }
                });
            }
        })
        .unwrap();

        Image {
            underlying,
            width: self.width,
            height: self.height,
            rgb: false,
        }
    }

    pub fn num_colours(&self) -> usize {
        if self.rgb {
            NUM_COLOURS_RGB
        } else {
            NUM_COLOURS_GREY
        }
    }

    pub fn remove_seam(&self, seam: &Seam) -> Image {
        if self.width == 0 {
            panic!("Cannot shrink zero wide/high image")
        } else if seam.len() != self.height {
            panic!("Seam length didn't match image height")
        } else {
            let width = self.width - 1;
            let height = self.height;
            let num_colours = self.num_colours();
            let num_elems = width * height;
            let mut underlying: Vec<Colour> = vec![0.0; num_elems*num_colours];
            let chunk_size = ((num_elems - 1 + NUM_THREADS) / NUM_THREADS)*num_colours;

            let chunks = underlying.chunks_mut(chunk_size);
            crossbeam::thread::scope(|scope| {
                for (chunk_idx, chunk) in chunks.enumerate() {
                    scope.spawn(move |_| {
                        for (elem_idx, elem) in chunk.iter_mut().enumerate() {
                                let global_idx = chunk_idx * chunk_size + elem_idx;
                                let elem_x = (global_idx/num_colours) % width;
                                let elem_y = (global_idx/num_colours) / width;
                                let colour = global_idx % num_colours;
                                let to_delete_x = *seam.get(elem_y)
                                    .expect("Y coordinate not found in seam");
                                let to_copy_x = if elem_x < to_delete_x {
                                    elem_x
                                } else {
                                    elem_x + 1
                                };
                                let to_copy_idx = (to_copy_x + elem_y * self.width)*num_colours + colour;
                                let old_elem =
                                    self.underlying.get(to_copy_idx).expect("you hecced up");
                                *elem = *old_elem;
                        }
                    });
                }
            })
            .expect("One of the threads crashed");

            Image {
                underlying,
                width,
                height,
                rgb: self.rgb,
            }
        }
    }

    // Sink node is the node with no outgoing edges. If there's more than 1, behaviour is undefined
    pub fn find_shortest_row(&self, hint: &Option<usize>) -> Seam {
        let mut visited_nodes = HashMap::new();
        let mut priority_queue = BinaryHeap::new();
        let eta = 1e-6;
        let first_node = Node {
            id: 0,
            distance: eta,
            previous: None,
        };
        // Useful for prioritising deeper nodes in cases where everything is equal
        let mult = 0.9999;

        let first_successors = first_node.successors(self.width, self.height);

        for successor_id in first_successors {
            let minimum_dist = if let Some(n) = hint {
                debug_assert_ne!(successor_id, first_node.id);
                debug_assert!(successor_id <= self.width);
                dampen(mult, successor_id-1, *n)
            } else {
               1. 
            } * eta;
            let adjusted_dist = minimum_dist + *self.underlying.get(successor_id-1).unwrap() as f64;
            let successor_node = Node {
                id: successor_id,
                distance: adjusted_dist,
                previous: Some(0)
            };
            visited_nodes.insert(successor_id, successor_node.clone());
            priority_queue.push(successor_node);
        };

        visited_nodes.insert(0, first_node);
        while !priority_queue.is_empty() {
            let current = priority_queue.pop().unwrap();
            let successors = current.successors(self.width, self.height);
            // Found the sink
            if successors.is_empty() {
                break;
            }

            for successor in successors {
                let alt_dist = if successor != self.width * self.height + 1 {
                    // if it's not the sink
                    let message = format!("Your algorithm sucks! {}", successor - 1);
                    current.distance*mult + *self.underlying.get(successor - 1).expect(&message) as f64
                } else {
                    // if it is the sink
                    0.0
                };
                match visited_nodes.get(&successor) {
                    Some(dist) if dist.distance >= alt_dist => (),
                    Some(_) | None => {
                        let node = Node {
                            id: successor,
                            distance: alt_dist,
                            previous: Some(current.id),
                        };
                        visited_nodes.insert(successor, node.clone());
                        priority_queue.push(node);
                    }
                };
            }
        }

        let mut result = vec![];
        let mut current_node = visited_nodes
            .get(&(self.width * self.height + 1))
            .expect("Last node should have been reached");
        let next_node_idx = current_node
            .previous
            .expect("Last node should have a predecessor");
        let mut next_node = visited_nodes
            .get(&next_node_idx)
            .expect("Previous node should have been visited");

        // backtrack until we find the beginning
        while next_node.id != 0 {
            current_node = next_node;
            let next_node_idx = current_node
                .previous
                .expect("Encountered a node with no previous node");
            next_node = visited_nodes
                .get(&next_node_idx)
                .expect("Encountered a node not visited");
            
            let next_x = (current_node.id - 1) % self.width;
            result.push(next_x);
        }
        result.reverse();
        result
    }

    pub fn combine<F>(&self, other: &Self, f: F) -> Image 
    where
        F: Fn(Colour, Colour) -> Colour + std::marker::Send + std::marker::Copy,
    {
        assert_eq!(self.width, other.width);
        assert_eq!(self.height, other.height);
        let size = self.width * self.height;

        let elems_per_thread = size/NUM_THREADS;
        let elems_for_last_thread = elems_per_thread+size%NUM_THREADS;

        // Output, with correct size
        let mut out_vec: Vec<Colour> = Vec::with_capacity(size);
        unsafe {
            out_vec.set_len(size)
        };

        let output_vec = Arc::new(out_vec);

        crossbeam::thread::scope(|scope| {
            for t in 0..NUM_THREADS {
                let chunk_size = if t + 1 == NUM_THREADS {
                    elems_for_last_thread
                } else {
                    elems_per_thread
                };
                let beginning = elems_per_thread * t;
                let end = beginning + chunk_size;
                debug_assert!(end <= size);

                let output_vec = Arc::clone(&output_vec);

                scope.spawn(move |_| unsafe {
                    let self_ptr = self.underlying.as_ptr();
                    let other_ptr = other.underlying.as_ptr();

                    // Hack in order to avoid interior mutability for Arc
                    let output = output_vec.as_ptr() as *mut f32;
                    for i in beginning..end {
                        let e1 = *self_ptr.offset(i as isize);
                        let e2 = *other_ptr.offset(i as isize);
                        *output.add(i) = f(e1, e2);
                    }
                });
            };
        }).unwrap();

        Image{underlying: Arc::try_unwrap(output_vec).unwrap(), ..*self}
    }

    pub fn transform<F>(&self, f: F) -> Image
    where
        F: Fn(Colour) -> Colour + std::marker::Send + std::marker::Copy,
    {
        let num_elems = self.height * self.width;
        let mut underlying: Vec<f32> = vec![0.0; num_elems];
        let chunk_size = (num_elems - 1 + NUM_THREADS) / NUM_THREADS;

        let chunks = underlying.chunks_mut(chunk_size);
        crossbeam::thread::scope(|scope| {
            for (chunk_idx, chunk) in chunks.enumerate() {
                scope.spawn(move |_| {
                    for (elem_idx, elem) in chunk.iter_mut().enumerate() {
                        let global_idx = chunk_idx * chunk_size + elem_idx;
                        *elem = f(unsafe { *self.underlying.get_unchecked(global_idx) });
                    }
                });
            }
        })
        .expect("One of the threads crashed");

        Image {
            underlying,
            width: self.width,
            height: self.height,
            rgb: false,
        }
    }

    pub fn save(&self, path: &str) -> Result<(), Box<dyn Error>> {
        let file = File::create(path)?;
        let ref mut w = BufWriter::new(file);

        let mut encoder = png::Encoder::new(w, self.width as u32, self.height as u32);
        if self.rgb {
            encoder.set_color(png::ColorType::RGB)
        } else {
            encoder.set_color(png::ColorType::Grayscale)
        };
        encoder.set_depth(png::BitDepth::Eight);
        let mut writer = encoder.write_header()?;
        let integer_data: Vec<_> = Image::vec_f32_to_u8(&self.underlying);
        writer.write_image_data(&integer_data)?;
        Ok(())
    }

    pub fn to_greyscale(&self) -> Image {
        let underlying = if self.rgb {
            assert!(self.underlying.len() % NUM_COLOURS_RGB == 0);
            assert!(self.underlying.len() / NUM_COLOURS_RGB == self.width * self.height);
            let mut underlying = Vec::with_capacity(self.underlying.len());
            let mut iterator = self.underlying.iter();
            while let Some(r_int) = iterator.next() {
                let r = *r_int as f32;
                let g = *iterator.next().unwrap() as f32;
                let b = *iterator.next().unwrap() as f32;
                let grey = 0.299 * r + 0.587 * g + 0.114 * b;
                underlying.push(grey as Colour);
            }
            underlying
        } else {
            self.underlying.clone()
        };

        Image {
            width: self.width,
            height: self.height,
            rgb: false,
            underlying,
        }
    }
}

fn dampen(to_dampen: f64, n1: usize, n2: usize) -> f64 {
    if n1 == n2 {
        to_dampen
    } else {
        1.
    }
}

