pub mod image;

#[macro_export]
macro_rules! err {
    ($x:expr) => {
        match $x {
            Err(e) => {
                eprintln!("An error occurred: {}", e);
                return;
            }
            Ok(ok) => ok,
        }
    };
}
